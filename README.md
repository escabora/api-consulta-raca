# TESTE ENEXT

## Building

* Clone the repo
* Install all dependencies by performing on the repo's folder:
  * `npm install`
  * `bower install`
* Now
  * `gulp` default task development
  * `gulp build --prod` task production
* Access http://localhost:3000
* 🚢

-----

### Credits

* [Susy](http://susy.oddbird.net/)
