function PegaDogs(getDog){
  
    //pegando dog e passando na url
    jQuery.getJSON( "https://dog.ceo/api/breed/" + getDog +"/images/random", function( data ) {
      jQuery(".box-image").html("<img src='" + data.message + "'>");
    });
  }

function CarregaDogs(){
  jQuery.getJSON( "https://dog.ceo/api/breeds/list/all", function( data ) {
    var breeds = data.message;
    firstDog = Object.keys(breeds)[0];

    jQuery.each(breeds, function(dog,breed){
      // se o dog tem sub raça        
      if (breeds[dog].length >= 1) {
        for (i = 0; i < breeds[dog].length; i++) { 
          jQuery("#select_raca .fild-filter").append('<li class="fild-filter__link" data-filter='+ breeds[dog][i] + ' '+ dog +' id="' + dog + '-'+ breeds[dog][i] +'">'+ breeds[dog][i] + ' '+ dog +'</li>');
        }
      }
          
      // sem sub raça
      else if (breeds[dog].length < 1 ) {
        // fazendo o lista
        jQuery("#select_raca .fild-filter").append('<li class="fild-filter__link" data-filter=' + dog + ' id="' + dog + '">' + dog + '</li>');
      }
    });
        
    jQuery.getJSON( "https://dog.ceo/api/breed/" + firstDog +"/images/random", function( data ) {
      jQuery(".box-image").html("<img src='" + data.message + "'>");
    });
  });            
}



jQuery(window).on('load', function() {


  setTimeout(function() {
    // seleciona a raca 
    jQuery("#select_raca li").click(function(e){
      e.preventDefault();
      getDog = jQuery(this).attr('id');

      jQuery(this).each(function() {
          PegaDogs(getDog);
      });
    });


    //montando select
    jQuery('.nav-filter').click(function(e) {
      e.preventDefault();
      jQuery(this).find('.btn-filter').toggleClass('is-active-arrow');
      jQuery(this).toggleClass('nav-filter--open');
      jQuery(this).find('.content-filter-scroll').toggleClass('is-active');
    });

    jQuery('.fild-filter__link').click(function(e) {
      e.preventDefault();
      var filter = jQuery(this).attr('data-filter');
      var textFild = jQuery(this).text();

      jQuery(this).parent().find('.btn-filter').removeClass('is-active-arrow');
      jQuery(this).removeClass('nav-filter--open');
      jQuery(this).parent().removeClass('is-active');
      jQuery(this).parent().parent().parent().find('.value-fild-filte').empty().html(textFild);
    });   
  }, 500);
  

});

// salvando dados no local storage
function salveUserLocation() {
  var racaDog = jQuery("#select_raca .value-fild-filte").text();
  var nomeDog = jQuery("input[name=dog-name]").val();
  var fontColor = jQuery("#select_color .value-fild-filte").text();
  var fontFamily = jQuery("#select_font .value-fild-filte").text();
  var d1 = new Date();

  localStorage.setItem("racaDog", racaDog);
  localStorage.setItem("nomeDog", nomeDog);
  localStorage.setItem("fontColor", fontColor);
  localStorage.setItem("fontFamily", fontFamily);
  localStorage.setItem("key", d1.getTime());

  setTimeout(function(){
    jQuery(".success-notifications").addClass("success-notifications--show");
  }, 400);
  
}


jQuery(document).ready(function(){
  CarregaDogs();
  
  // select para escolher a cor
  jQuery("#select_color li").click(function(e){
    e.preventDefault();
    var selectedColor = jQuery(this).attr('data-filter');
    jQuery(".box-image__name").attr('data-color', selectedColor);

  });

  // select para escolher a fonte
  jQuery("#select_font li").click(function(e){
    e.preventDefault();
    var selectedFont = jQuery(this).attr('data-filter');

    jQuery(".box-image__name").attr('data-font', selectedFont);

  });

  // digita o nome do dog
  jQuery("input[name=dog-name]").keyup(function(){
    var nameDog = jQuery(this).val();
    jQuery(".box-image__name h2").text("").html(nameDog);
  });

  // fecha notificacao
  jQuery('.close-notifications').click(function(e) {
    e.preventDefault();
    jQuery(".success-notifications").removeClass("success-notifications--show");
  });

});



